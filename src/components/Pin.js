import React, { Component } from 'react';
import '../App.css';
import fontawesome from '@fortawesome/fontawesome'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'


const MARKER_SIZE = 20;
const pinStyle = {
  position: 'absolute',
  width: MARKER_SIZE,
  height: MARKER_SIZE,
  left: -MARKER_SIZE / 2,
  top: -MARKER_SIZE * 1.2
}

class Pin extends Component {

    constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.toggleModal = this.toggleModal.bind(this);


  }

toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  } 


  render() {




var {
        id,
        postTitle,
        lat,
        lng
    } = this.props.data;

    return (

        <div className="pin" id={id} style={pinStyle} title={postTitle} onClick={this.props.action}>
        
        <svg aria-hidden="true" data-prefix="fas" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" className="svg-inline--fa fa-map-marker-alt fa-w-12 fa-3x"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z" ></path></svg>
        </div>

    );
  }
}

export default Pin;