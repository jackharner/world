import React, { Component } from 'react';
import '../App.css';

class Header extends Component {
  render() {
    return (

        <header className="App-header">
          <h1 className="App-title">WorldVu</h1>
          <nav className="header__nav">
          	<ul>
          	<li><a href="#">Locations</a></li>
          	<li><a href="#">Writers</a></li>
          	<li><a href="#">Login</a></li>
          	<li className="outline"><a href="#">Sign Up</a></li>
          	</ul>
          </nav>
        </header>

    );
  }
}

export default Header;
