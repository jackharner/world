import React, { Component } from 'react';
import '../App.css';

class Post extends Component {
  render() {

    var {
        id,
        postTitle,
        lat,
        lng
    } = this.props.data;


    return (

        <div className="post" id="{id}">
          <h1>{postTitle}</h1>
        </div>

    );
  }
}

export default Post;