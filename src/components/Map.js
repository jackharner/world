import React, { Component } from 'react';
import '../App.css';
import Pin from './Pin.js'
import Modal from './Modal.js'
import GoogleMapReact from 'google-map-react'



export default class Map extends Component {

static defaultProps = {
    center: { lat: 40.7446790, lng: -73.9485420 },
    zoom: 3
  }

  constructor(props) {
            super(props);
            this.state = {
                              isOpen: false,

                data: [
                    {
                        id: "1",
                        postTitle: "Albuquerque",
                        lat: 35.106766,
                        lng: -106.629181
                    },
                    {
                        id: "12",
                        postTitle: "Somewhere",
                        lat: 45.106766,
                        lng: -106.629181
                    },
                    {
                       id: "2",
                        postTitle: "Article 2",
                        lat: "10",
                        lng: "100"
                    },
                    {
                        id: "3",
                        postTitle: "Article 3",
                        lat: "100",
                        lng: "10"
                    }
                ],
            };
        }



toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });



  }


  render() {

    var pins = this.state.data.map((post) =>
        <Pin lat={post.lat} lng={post.lng} data={post} key={post.id} action={this.toggleModal} />
    );

    return (
      <div className='google-map'style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          defaultCenter={ this.props.center }
          defaultZoom={ this.props.zoom }>


          

          {pins}

        </GoogleMapReact>

              <Modal show={this.state.isOpen}
          onClose={this.toggleModal}>
          Here's some content for the modal
        </Modal>
      </div>
    )
  }
}

