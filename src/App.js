import React, { Component } from 'react';
import './App.css';


import fontawesome from '@fortawesome/fontawesome'
// import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import brands from '@fortawesome/fontawesome-free-brands'
import faMapMarkerAlt from '@fortawesome/fontawesome-free-solid/faMapMarkerAlt'
 
import Header from './components/Header.js';
import Map from './components/Map.js';

 

class App extends Component {

constructor(props) {
    super(props);

    this.state = { isOpen: false };
  }


  render() {
fontawesome.library.add(brands, faMapMarkerAlt);
    return (
      <div className="App">
        <Header />
        <Map />

      </div>
    );
  }
}

export default App;
